package xyz.naomieow.asbestos.damage;


import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.damage.DamageType;
import net.minecraft.registry.RegistryKey;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;
import xyz.naomieow.asbestos.AsbestosMod;

public class ModDamageTypes {
    public static final RegistryKey<DamageType> MESOTHELIOMA = RegistryKey.of(RegistryKeys.DAMAGE_TYPE, new Identifier(AsbestosMod.MOD_ID, "mesothelioma"));
    public static final RegistryKey<DamageType> BLOOD_EXTRACT = RegistryKey.of(RegistryKeys.DAMAGE_TYPE, new Identifier(AsbestosMod.MOD_ID, "blood_extract"));

    public static DamageSource of(World world, RegistryKey<DamageType> key) {
        return new DamageSource(world.getRegistryManager().get(RegistryKeys.DAMAGE_TYPE).entryOf(key));
    }

}
