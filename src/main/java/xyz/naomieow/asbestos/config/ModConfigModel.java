package xyz.naomieow.asbestos.config;

import io.wispforest.owo.config.annotation.Config;
import io.wispforest.owo.config.annotation.Modmenu;
import io.wispforest.owo.config.annotation.RangeConstraint;
import xyz.naomieow.asbestos.AsbestosMod;

@Modmenu(modId = AsbestosMod.MOD_ID)
@Config(name = "asbestos_config", wrapperName = "AsbestosConfig")
public class ModConfigModel {
    @RangeConstraint(min = 0, max = 32)
    public int offset = 3;
    public int period = 5;
    public int threshold = 180;
    public int exsanguination = 1;
}
