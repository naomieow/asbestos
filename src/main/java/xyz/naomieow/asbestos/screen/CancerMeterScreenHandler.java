package xyz.naomieow.asbestos.screen;

import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.screen.slot.Slot;
import xyz.naomieow.asbestos.inventory.CancerMeterInventory;
import xyz.naomieow.asbestos.inventory.ICancerMeterInventory;
import xyz.naomieow.asbestos.item.CancerMeterItem;
import xyz.naomieow.asbestos.item.ModItems;

import java.util.Objects;

public class CancerMeterScreenHandler extends ScreenHandler {
    private final int INVENTORY_START = 1, INVENTORY_END = 28, HOTBAR_START = 28, HOTBAR_END = 37;

    public PlayerInventory playerInventory;
    public ICancerMeterInventory inventory;

    public CancerMeterScreenHandler(int syncId, PlayerInventory playerInventory, PacketByteBuf buf) {
        this(syncId, playerInventory, createInventory(playerInventory, buf));
    }

    private CancerMeterScreenHandler(ScreenHandlerType<?> type, int syncId, PlayerInventory playerInventory, ICancerMeterInventory inventory) {
        super(type, syncId);
        this.playerInventory = playerInventory;
        this.inventory = inventory;

        this.addSlot(new Slot(inventory.getInventory(), 0, 80, 54) {
            @Override
            public boolean canInsert(ItemStack stack) {
                return stack.isOf(ModItems.FILLED_SYRINGE);
            }
        });

        this.addPlayerInventory(playerInventory);
        this.addPlayerHotbar(playerInventory);
    }

    public CancerMeterScreenHandler(int syncId, PlayerInventory playerInventory, ICancerMeterInventory inventory) {
        this(ModHandlers.CANCER_METER, syncId, playerInventory, inventory);
    }

    private static CancerMeterInventory createInventory(final PlayerInventory playerInventory, final PacketByteBuf buf) {
        Objects.requireNonNull(playerInventory);
        Objects.requireNonNull(buf);

        final ItemStack stack;
        final byte screenID = buf.readByte();

        if (screenID == 1) {
            stack = playerInventory.player.getEquippedStack(EquipmentSlot.MAINHAND);
        } else {
            stack = ItemStack.EMPTY;
        }

        if (stack.getItem() instanceof CancerMeterItem) {
            if (screenID == 1) {
                return new CancerMeterInventory(stack, playerInventory.player, screenID);
            }
        }
        throw new IllegalStateException("Incorrect ItemStack " + stack);
    }


    @Override
    public ItemStack quickMove(PlayerEntity player, int slotIndex) {
        ItemStack itemStack = ItemStack.EMPTY;
        Slot slot = this.slots.get(slotIndex);
        if (slot.hasStack()) {
            ItemStack stackInSlot = slot.getStack();
            itemStack = stackInSlot.copy();
            if (slotIndex == 0) {
                if (!this.insertItem(stackInSlot, INVENTORY_START, HOTBAR_END, true)) {
                    return ItemStack.EMPTY;
                }
                slot.onQuickTransfer(stackInSlot, itemStack);
            } else if (itemStack.isOf(ModItems.FILLED_SYRINGE)) {
                if (!this.insertItem(stackInSlot, 0, 1, false)) {
                    return ItemStack.EMPTY;
                }
            } else if (slotIndex >= INVENTORY_START && slotIndex < INVENTORY_END) {
                if (!this.insertItem(stackInSlot, HOTBAR_START, HOTBAR_END, false)) {
                    return ItemStack.EMPTY;
                }
            } else if (slotIndex >= HOTBAR_START && slotIndex < HOTBAR_END) {
                if (!this.insertItem(stackInSlot, INVENTORY_START, INVENTORY_END, false)) {
                    return ItemStack.EMPTY;
                }
            } else {
                if (!this.insertItem(stackInSlot, INVENTORY_START, HOTBAR_END, false)) {
                    return ItemStack.EMPTY;
                }
            }

            if (stackInSlot.isEmpty()) {
                slot.setStack(ItemStack.EMPTY);
            } else {
                slot.markDirty();
            }

            if (stackInSlot.getCount() == itemStack.getCount()) {
                return ItemStack.EMPTY;
            }

            slot.onTakeItem(player, stackInSlot);

            if (slotIndex == 0) {
                player.dropItem(stackInSlot, false);
            }
        }
        return itemStack;
    }

    @Override
    public boolean canUse(PlayerEntity player) {
        return true;
    }

    private void addPlayerInventory(PlayerInventory inventory) {
        for (int row = 0; row < 3; ++row) {
            for (int column = 0; column < 9; ++column) {
                this.addSlot(new Slot(inventory, column + row * 9 + 9, 8 + column * 18, 84 + row * 18));
            }
        }
    }

    private void addPlayerHotbar(PlayerInventory inventory) {
        for (int slot = 0; slot < 9; ++slot) {
            this.addSlot(new Slot(inventory, slot, 8 + slot * 18, 142));
        }
    }

    public int getCancerMeter() {
        NbtCompound nbt = this.inventory.getInventory().getStack(0).getOrCreateNbt();
        return nbt.contains("CancerMeter") ? nbt.getInt("CancerMeter") : 0;
    }
}
