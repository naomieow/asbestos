package xyz.naomieow.asbestos.screen;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import xyz.naomieow.asbestos.AsbestosMod;
import xyz.naomieow.asbestos.screen.util.MouseUtil;

import java.util.List;
import java.util.Optional;

@Environment(EnvType.CLIENT)
public class CancerMeterScreen extends HandledScreen<CancerMeterScreenHandler> {
    private static final Identifier TEXTURE = new Identifier(AsbestosMod.MOD_ID, "textures/gui/cancer_meter.png");
    CancerMeterScreenHandler screenHandler;

    public CancerMeterScreen(CancerMeterScreenHandler handler, PlayerInventory inventory, Text title) {
        super(handler, inventory, title);
        screenHandler = handler;
    }
    //109, 56 -- 176, 56 -- 58x7

    @Override
    protected void drawBackground(DrawContext context, float delta, int mouseX, int mouseY) {
        int x = (width - backgroundWidth) / 2;
        int y = (height - backgroundHeight) / 2;
        context.drawTexture(TEXTURE, x, y, 0, 0, backgroundWidth, backgroundHeight);
        drawCancerProgress(context, x + 109, y + 56);
    }

    @Override
    protected void drawForeground(DrawContext context, int mouseX, int mouseY) {
        int x = (width - backgroundWidth) / 2;
        int y = (height - backgroundHeight) / 2;

        if (isMouseAboveArea(mouseX, mouseY, x, y, 108, 55, 58, 7)) {
            context.drawTooltip(
                    textRenderer,
                    List.of(Text.literal(screenHandler.getCancerMeter() + "/" + AsbestosMod.CONFIG.threshold())),
                    Optional.empty(),
                    mouseX - x,
                    mouseY - y
            );
        }
        context.drawText(this.textRenderer, this.playerInventoryTitle, this.playerInventoryTitleX, this.playerInventoryTitleY + 2, 4210752, false);
    }

    private void drawCancerProgress(DrawContext context, int x, int y) {
        int offset = screenHandler.getCancerMeter() * 58 / AsbestosMod.CONFIG.threshold();
        context.drawTexture(TEXTURE,
                x, y,
                176, 56,
                offset, 7);
    }

    @Override
    public void render(DrawContext context, int mouseX, int mouseY, float delta) {
        renderBackground(context);
        super.render(context, mouseX, mouseY, delta);
        drawMouseoverTooltip(context, mouseX, mouseY);
    }

    private boolean isMouseAboveArea(
            int mouseX, int mouseY,
            int x, int y,
            int offsetX, int offsetY,
            int width, int height
    ) {
        return MouseUtil.isMouseOver(
                mouseX, mouseY,
                x + offsetX, y + offsetY,
                width, height
        );
    }

}
