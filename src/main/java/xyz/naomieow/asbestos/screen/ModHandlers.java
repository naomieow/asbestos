package xyz.naomieow.asbestos.screen;

import io.wispforest.owo.registration.reflect.AutoRegistryContainer;
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerType;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.screen.ScreenHandlerType;

public class ModHandlers implements AutoRegistryContainer<ScreenHandlerType<?>> {
    public static final ScreenHandlerType<CancerMeterScreenHandler> CANCER_METER =
            new ExtendedScreenHandlerType<>(CancerMeterScreenHandler::new);


    @Override
    public Registry<ScreenHandlerType<?>> getRegistry() {
        return Registries.SCREEN_HANDLER;
    }

    @Override
    public Class<ScreenHandlerType<?>> getTargetFieldType() {
        return (Class<ScreenHandlerType<?>>) (Object) ScreenHandlerType.class;
    }

}
