package xyz.naomieow.asbestos.mesothelioma;

import dev.onyxstudios.cca.api.v3.component.sync.AutoSyncedComponent;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.NbtCompound;
import xyz.naomieow.asbestos.AsbestosMod;

public class PlayerMesoComponet implements MesoComponent, AutoSyncedComponent {
    private PlayerEntity playerEntity;
    private int mesothelioma;

    public PlayerMesoComponet(PlayerEntity playerEntity) {
        this.playerEntity = playerEntity;
        this.mesothelioma = 0;
    }

    @Override
    public int getMesothelioma() {
        return this.mesothelioma;
    }

    @Override
    public void setMesothelioma(int exposure) {
        this.mesothelioma = exposure;
        AsbestosMod.MESOTHELIOMA.sync(playerEntity);
    }

    @Override
    public void readFromNbt(NbtCompound tag) {
        this.mesothelioma = tag.getInt("mesothelioma");
    }

    @Override
    public void writeToNbt(NbtCompound tag) {
        tag.putInt("mesothelioma", this.mesothelioma);
    }
}
