package xyz.naomieow.asbestos.mesothelioma;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectCategory;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.entity.player.PlayerEntity;
import xyz.naomieow.asbestos.AsbestosMod;
import xyz.naomieow.asbestos.damage.ModDamageTypes;

public class MesoStatusEffect extends StatusEffect {
    public MesoStatusEffect() {
        super(StatusEffectCategory.HARMFUL, 0x302412);
    }

    @Override
    public boolean canApplyUpdateEffect(int duration, int amplifier) {
        return true;
    }

    @Override
    public void applyUpdateEffect(LivingEntity entity, int amplifier) {
        super.applyUpdateEffect(entity, amplifier);
        if (entity instanceof PlayerEntity playerEntity) {
            int exposure = AsbestosMod.MESOTHELIOMA.get(playerEntity).getMesothelioma();
            if (exposure >= (AsbestosMod.CONFIG.threshold() / 1.5)) {
                playerEntity.addStatusEffect(new StatusEffectInstance(StatusEffects.SLOWNESS, -1, 1));
            }

            if (exposure >= AsbestosMod.CONFIG.threshold()) {
                playerEntity.damage(ModDamageTypes.of(playerEntity.getWorld(), ModDamageTypes.MESOTHELIOMA), 1 << amplifier);
            }
        }
    }
}
