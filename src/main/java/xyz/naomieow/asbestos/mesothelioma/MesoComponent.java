package xyz.naomieow.asbestos.mesothelioma;

import dev.onyxstudios.cca.api.v3.component.ComponentV3;

public interface MesoComponent extends ComponentV3 {
    int getMesothelioma();
    void setMesothelioma(int exposure);
}
