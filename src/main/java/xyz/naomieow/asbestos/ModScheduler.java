package xyz.naomieow.asbestos;

import net.fabricmc.fabric.api.event.lifecycle.v1.ServerTickEvents;
import net.minecraft.block.Block;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import xyz.naomieow.asbestos.block.CarcinogenicBlock;

import java.util.Arrays;

public class ModScheduler {
    public static void init() {
        ServerTickEvents.END_SERVER_TICK.register( server -> {
            if (server.getTicks() % (AsbestosMod.CONFIG.period() * 20) == 0) {
                server.getWorlds().forEach(serverWorld -> serverWorld.getPlayers().forEach(serverPlayerEntity -> {
                    int exposure = AsbestosMod.MESOTHELIOMA.get(serverPlayerEntity).getMesothelioma();

                    if (exposure >= (AsbestosMod.CONFIG.threshold() / 2)) {
                        serverPlayerEntity.addStatusEffect(new StatusEffectInstance(AsbestosMod.MESO_STATUS, -1));
                    }

                    if (CarcinogenicBlock.isProtectedFromAsbestos(serverPlayerEntity)) {
                        return;
                    }

                    int offset = AsbestosMod.CONFIG.offset();

                    findasbestos: for (int i = -offset; i < offset; i++) {
                        for (int j = -offset; j < offset; j++) {
                            for (int k = -offset; k < offset; k++) {
                                Vec3d offsetPos = serverPlayerEntity.getPos().add(i, j, k);
                                BlockPos pos = new BlockPos((int) offsetPos.getX(), (int) offsetPos.getY(), (int) offsetPos.getZ());
                                Block block = serverWorld.getBlockState(pos).getBlock();
                                if (Arrays.asList(AsbestosMod.HARMFUL_PASSIVE_BLOCKS).contains(block)) {
                                    AsbestosMod.MESOTHELIOMA.get(serverPlayerEntity).setMesothelioma(exposure + 1);
                                    break findasbestos;
                                }
                            }
                        }
                    }
                }));
            }
        });
    }
}
