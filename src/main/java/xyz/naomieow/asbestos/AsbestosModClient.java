package xyz.naomieow.asbestos;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.gui.screen.ingame.HandledScreens;
import xyz.naomieow.asbestos.screen.CancerMeterScreen;
import xyz.naomieow.asbestos.screen.ModHandlers;

@Environment(EnvType.CLIENT)
public class AsbestosModClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        HandledScreens.register(ModHandlers.CANCER_METER, CancerMeterScreen::new);
    }
}
