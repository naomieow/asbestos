package xyz.naomieow.asbestos;

import dev.onyxstudios.cca.api.v3.component.ComponentKey;
import dev.onyxstudios.cca.api.v3.component.ComponentRegistryV3;
import dev.onyxstudios.cca.api.v3.entity.EntityComponentFactoryRegistry;
import dev.onyxstudios.cca.api.v3.entity.EntityComponentInitializer;
import dev.onyxstudios.cca.api.v3.entity.RespawnCopyStrategy;
import io.wispforest.owo.registration.reflect.FieldRegistrationHandler;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.biome.v1.BiomeModifications;
import net.fabricmc.fabric.api.biome.v1.BiomeSelectors;
import net.minecraft.block.Block;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.registry.*;
import net.minecraft.util.Identifier;
import net.minecraft.world.gen.GenerationStep;

import net.minecraft.world.gen.feature.*;
import xyz.naomieow.asbestos.block.ModBlocks;
import xyz.naomieow.asbestos.config.AsbestosConfig;
import xyz.naomieow.asbestos.item.ModGroups;
import xyz.naomieow.asbestos.item.ModItems;
import xyz.naomieow.asbestos.mesothelioma.MesoComponent;
import xyz.naomieow.asbestos.mesothelioma.MesoStatusEffect;
import xyz.naomieow.asbestos.mesothelioma.PlayerMesoComponet;
import xyz.naomieow.asbestos.screen.ModHandlers;

public class AsbestosMod implements ModInitializer, EntityComponentInitializer {
    public static final String MOD_ID = "asbestos";
    public static final ComponentKey<MesoComponent> MESOTHELIOMA = ComponentRegistryV3.INSTANCE
            .getOrCreate(new Identifier(MOD_ID, "mesothelioma"), MesoComponent.class);
    public static final AsbestosConfig CONFIG = AsbestosConfig.createAndLoad();
    public static final StatusEffect MESO_STATUS = new MesoStatusEffect();
    public static final Block[] HARMFUL_PASSIVE_BLOCKS = { ModBlocks.ASBESTOS_BLOCK};
    public static final RegistryKey<PlacedFeature> SERPENTINITE_PLACED_KEY = RegistryKey.of(RegistryKeys.PLACED_FEATURE, new Identifier(MOD_ID, "serpentinite"));



    @Override
    public void registerEntityComponentFactories(EntityComponentFactoryRegistry registry) {
        registry.registerForPlayers(MESOTHELIOMA, PlayerMesoComponet::new, RespawnCopyStrategy.NEVER_COPY);
    }

    @Override
    public void onInitialize() {
        FieldRegistrationHandler.register(ModItems.class, MOD_ID, false);
        FieldRegistrationHandler.register(ModBlocks.class, MOD_ID, false);
        FieldRegistrationHandler.register(ModHandlers.class, MOD_ID, false);
        Registry.register(Registries.STATUS_EFFECT, new Identifier(MOD_ID, "mesothelioma"), MESO_STATUS);
        BiomeModifications.addFeature(BiomeSelectors.foundInOverworld(), GenerationStep.Feature.UNDERGROUND_ORES, SERPENTINITE_PLACED_KEY);
        ModGroups.init();
        ModScheduler.init();
    }
}
