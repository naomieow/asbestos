package xyz.naomieow.asbestos.block;

import io.wispforest.owo.itemgroup.OwoItemSettings;
import io.wispforest.owo.registration.reflect.BlockRegistryContainer;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.item.BlockItem;
import xyz.naomieow.asbestos.item.ModGroups;

public class ModBlocks implements BlockRegistryContainer {
    public static final Block ASBESTOS_BLOCK = new CarcinogenicBlock(
            FabricBlockSettings.copyOf(Blocks.WHITE_WOOL).hardness(1.0f), 40);
    public static final Block SERPENTINITE_BLOCK = new CarcinogenicBlock(
            FabricBlockSettings.copyOf(Blocks.STONE).hardness(3.0f).requiresTool(), 20);
    public static final Block POPCORN_CEILING_BLOCK = new CarcinogenicBlock(
            FabricBlockSettings.copyOf(Blocks.OAK_PLANKS).hardness(2.0f), 20);
    public static final Block ASBESTOS_TILE_BLOCK = new CarcinogenicBlock(
            FabricBlockSettings.copyOf(Blocks.NETHER_BRICKS).hardness(3.0f), 20);
    public static final Block ASBESTOS_ROOF_BLOCK = new AsbestosRoofBlock(
            FabricBlockSettings.copyOf(ASBESTOS_TILE_BLOCK).nonOpaque(), 20);

    @Override
    public BlockItem createBlockItem(Block block, String identifier) {
        return new BlockItem(block, new OwoItemSettings().group(ModGroups.ASBESTOS_GROUP));
    }
}
