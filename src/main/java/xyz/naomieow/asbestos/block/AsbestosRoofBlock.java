package xyz.naomieow.asbestos.block;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.ShapeContext;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.Properties;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.BlockView;
import org.jetbrains.annotations.Nullable;

public class AsbestosRoofBlock extends CarcinogenicBlock {
    public AsbestosRoofBlock(Settings settings, int dangerLevel) {
        super(settings, dangerLevel);
        setDefaultState(this.getStateManager().getDefaultState().with(Properties.HORIZONTAL_FACING, Direction.NORTH));
        if (FabricLoader.getInstance().getEnvironmentType() != EnvType.SERVER) {
            BlockRenderLayerMap.INSTANCE.putBlock(this, RenderLayer.getCutout());
        }
    }

    @Override
    protected void appendProperties(StateManager.Builder<Block, BlockState> builder) {
        builder.add(Properties.HORIZONTAL_FACING);
    }

    @Override
    public VoxelShape getOutlineShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context) {

        Direction dir = state.get(Properties.HORIZONTAL_FACING);
        return switch (dir) {
            case NORTH -> VoxelShapes.union(VoxelShapes.cuboid(0, 0, 0, 1, 0.5, 1),
                    VoxelShapes.cuboid(0, 0.5, 0.5, 1, 1, 1));
            case SOUTH -> VoxelShapes.union(VoxelShapes.cuboid(0, 0, 0, 1, 0.5, 1),
                    VoxelShapes.cuboid(0, 0.5, 0, 1, 1, 0.5));
            case EAST -> VoxelShapes.union(VoxelShapes.cuboid(0, 0, 0, 1, 0.5, 1),
                    VoxelShapes.cuboid(0, 0.5, 0, 0.5, 1, 1));
            case WEST -> VoxelShapes.union(VoxelShapes.cuboid(0, 0, 0, 1, 0.5, 1),
                    VoxelShapes.cuboid(0.5, 0.5, 0, 1, 1, 1));
            default -> VoxelShapes.fullCube();
        };
    }

    @Nullable
    @Override
    public BlockState getPlacementState(ItemPlacementContext ctx) {
        return this.getDefaultState().with(Properties.HORIZONTAL_FACING,
                ctx.getHorizontalPlayerFacing().getOpposite());
    }

    @Environment(EnvType.CLIENT)
    @Override
    public float getAmbientOcclusionLightLevel(BlockState state, BlockView world, BlockPos pos) {
        return 1.0f;
    }
}
