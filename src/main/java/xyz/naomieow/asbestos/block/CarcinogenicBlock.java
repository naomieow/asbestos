package xyz.naomieow.asbestos.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import xyz.naomieow.asbestos.AsbestosMod;
import xyz.naomieow.asbestos.item.ModItems;

import java.util.Arrays;

public class CarcinogenicBlock extends Block {
    private int dangerLevel;
    public static Item[] PROTECTIVE_ARMOUR = {ModItems.PPE_HELMET, ModItems.PPE_CHESTPLATE, ModItems.PPE_LEGGINGS, ModItems.PPE_BOOTS};

    public CarcinogenicBlock(Settings settings, int dangerLevel) {
        super(settings);
        this.dangerLevel = dangerLevel;
    }

    public static boolean isProtectedFromAsbestos(PlayerEntity player) {
        for (ItemStack item : player.getArmorItems()) {
            if (!Arrays.asList(PROTECTIVE_ARMOUR).contains(item.getItem())) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void onBreak(World world, BlockPos pos, BlockState state, PlayerEntity player) {
        super.onBreak(world, pos, state, player);
        if (!isProtectedFromAsbestos(player)) {
            int exposure = AsbestosMod.MESOTHELIOMA.get(player).getMesothelioma();
            AsbestosMod.MESOTHELIOMA.get(player).setMesothelioma(exposure + dangerLevel);
        }
    }
}
