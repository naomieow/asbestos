package xyz.naomieow.asbestos.item;

import io.wispforest.owo.itemgroup.OwoItemSettings;
import io.wispforest.owo.registration.reflect.ItemRegistryContainer;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.FoodComponent;
import net.minecraft.item.Item;
import net.minecraft.item.ToolItem;
import xyz.naomieow.asbestos.armour.ProtectiveMaterial;

public class ModItems implements ItemRegistryContainer {
    public static final Item ASBESTOS_FIBERS = new Item(
            new OwoItemSettings()
                    .group(ModGroups.ASBESTOS_GROUP)
                    .fireproof()
                    .food(new FoodComponent.Builder().alwaysEdible().statusEffect(
                            new StatusEffectInstance(StatusEffects.WITHER, 30 * 20, 4, false, false), 1.0f)
                            .build()));

    public static final ToolItem IRON_SCRAPER = new ScraperItem(ScraperItemMaterial.INSTANCE, 0.0f, -3.0f,
            new OwoItemSettings().group(ModGroups.ASBESTOS_GROUP));

    public static final Item FILLED_SYRINGE = new FilledSyringeItem(new OwoItemSettings().group(ModGroups.ASBESTOS_GROUP).maxCount(1));
    public static final Item EMPTY_SYRINGE = new EmptySyringeItem(new OwoItemSettings().group(ModGroups.ASBESTOS_GROUP).maxCount(16));
    public static final Item CANCER_METER = new CancerMeterItem(new OwoItemSettings().group(ModGroups.ASBESTOS_GROUP).maxCount(1));

    public static final Item PPE_HELMET = new ArmorItem(
            ProtectiveMaterial.INSTANCE, ArmorItem.Type.HELMET,
            new OwoItemSettings().group(ModGroups.ASBESTOS_GROUP)
    );
    public static final Item PPE_CHESTPLATE = new ArmorItem(
            ProtectiveMaterial.INSTANCE, ArmorItem.Type.CHESTPLATE,
            new OwoItemSettings().group(ModGroups.ASBESTOS_GROUP)
    );
    public static final Item PPE_LEGGINGS = new ArmorItem(
            ProtectiveMaterial.INSTANCE, ArmorItem.Type.LEGGINGS,
            new OwoItemSettings().group(ModGroups.ASBESTOS_GROUP)
    );
    public static final Item PPE_BOOTS = new ArmorItem(
            ProtectiveMaterial.INSTANCE, ArmorItem.Type.BOOTS,
            new OwoItemSettings().group(ModGroups.ASBESTOS_GROUP)
    );
}
