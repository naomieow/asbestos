package xyz.naomieow.asbestos.item;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import xyz.naomieow.asbestos.AsbestosMod;

public class FilledSyringeItem extends Item {
    private static final String TYPE_KEY = "EntityType";
    private static final String CANCER_KEY = "CancerMeter";
    public FilledSyringeItem(Settings settings) {
        super(settings);
    }


    public static void writeNbt(LivingEntity target, ItemStack syringe) {
        NbtCompound nbt = syringe.getOrCreateNbt();
        syringe.setCustomName(Text.translatable("item.asbestos.filled_syringe", target.getName()).formatted(Formatting.RESET));
        nbt.putString(TYPE_KEY, EntityType.getId(target.getType()).toString());
        if (target instanceof PlayerEntity player) {
            nbt.putInt(CANCER_KEY, AsbestosMod.MESOTHELIOMA.get(player).getMesothelioma());
        }
    }
}
