package xyz.naomieow.asbestos.item;

import net.minecraft.block.Block;
import net.minecraft.item.MiningToolItem;
import net.minecraft.item.ToolMaterial;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.util.Identifier;
import xyz.naomieow.asbestos.AsbestosMod;

public class ScraperItem extends MiningToolItem {
    private static TagKey<Block> affectedBlocks = TagKey.of(RegistryKeys.BLOCK, new Identifier(AsbestosMod.MOD_ID, "scrapable"));

    public ScraperItem(ToolMaterial material, float attackDamage, float attackSpeed, Settings settings) {
        super(attackDamage, attackSpeed, material, affectedBlocks, settings);
    }
}
