package xyz.naomieow.asbestos.item;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;
import xyz.naomieow.asbestos.inventory.CancerMeterInventory;

public class CancerMeterItem extends Item {
    public CancerMeterItem(Settings settings) {
        super(settings);
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        ItemStack itemStack = user.getStackInHand(hand);
        if (hand == Hand.OFF_HAND) {
            return TypedActionResult.fail(itemStack);
        }

        if (!world.isClient()) {
            CancerMeterInventory.openHandledScreen(user, user.getMainHandStack(), (byte) 1);
        }
        return TypedActionResult.success(itemStack, world.isClient());
    }
}
