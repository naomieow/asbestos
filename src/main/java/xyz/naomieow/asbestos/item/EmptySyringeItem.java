package xyz.naomieow.asbestos.item;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;
import xyz.naomieow.asbestos.AsbestosMod;
import xyz.naomieow.asbestos.damage.ModDamageTypes;

public class EmptySyringeItem extends Item {
    public EmptySyringeItem(Settings settings) {
        super(settings);
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        useEmptySyringe(user, user, hand, world);
        return TypedActionResult.consume(user.getStackInHand(hand));
    }

    @Override
    public ActionResult useOnEntity(ItemStack stack, PlayerEntity user, LivingEntity entity, Hand hand) {
        World world = user.getWorld();
        useEmptySyringe(user, entity, hand, world);
        return ActionResult.CONSUME;
    }


    private void useEmptySyringe(PlayerEntity user, LivingEntity target, Hand hand, World world) {
        world.playSound(null, user.getBlockPos(), SoundEvents.ITEM_BOTTLE_FILL, SoundCategory.PLAYERS, 1.0F, 1.0F);
        ItemStack handStack = user.getStackInHand(hand);
        ItemStack newStack = new ItemStack(ModItems.FILLED_SYRINGE, 1);
        newStack.setNbt(handStack.getOrCreateNbt().copy());
        FilledSyringeItem.writeNbt(target, newStack);
        target.damage(ModDamageTypes.of(user.getWorld(), ModDamageTypes.BLOOD_EXTRACT), AsbestosMod.CONFIG.exsanguination());
        if (!user.getInventory().insertStack(newStack)) {
            user.dropItem(newStack, false);
        }
        if (!user.getAbilities().creativeMode) {
            handStack.decrement(1);
        }
    }
}
