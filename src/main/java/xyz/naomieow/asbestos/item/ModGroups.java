package xyz.naomieow.asbestos.item;

import io.wispforest.owo.itemgroup.Icon;
import io.wispforest.owo.itemgroup.OwoItemGroup;
import net.minecraft.util.Identifier;
import xyz.naomieow.asbestos.AsbestosMod;

public class ModGroups {
    public static final OwoItemGroup ASBESTOS_GROUP = OwoItemGroup
            .builder(new Identifier(AsbestosMod.MOD_ID, "asbestos_group"), () -> Icon.of(ModItems.ASBESTOS_FIBERS))
            .build();

    public static void init() {
        ASBESTOS_GROUP.initialize();
    }
}
