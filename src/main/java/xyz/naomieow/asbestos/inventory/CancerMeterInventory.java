package xyz.naomieow.asbestos.inventory;

import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtList;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import xyz.naomieow.asbestos.screen.CancerMeterScreenHandler;

public class CancerMeterInventory implements ICancerMeterInventory {
    private InventoryImproved inventory = createInventory(1);
    private final PlayerEntity player;
    private final byte screenID;
    private ItemStack stack;


    public CancerMeterInventory(ItemStack stack, PlayerEntity player, byte screenID) {
        this.player = player;
        this.stack = stack;
        this.screenID = screenID;

        if (stack != null){
            this.readAllData(stack.getOrCreateNbt());
        }
    }

    public void setStack(ItemStack stack) {
        this.stack = stack;
    }

    @Override
    public InventoryImproved getInventory() {
        return this.inventory;
    }

    @Override
    public void writeAllData(NbtCompound nbt) {
        this.writeItems(nbt);
    }

    @Override
    public void readAllData(NbtCompound nbt) {
        this.readItems(nbt);
    }

    @Override
    public void writeItems(NbtCompound nbt) {
        CancerMeterInventory.writeNbt(nbt, this.inventory.getStacks(), true);
    }

    @Override
    public void readItems(NbtCompound nbt) {
        this.inventory = createInventory(1);
        CancerMeterInventory.readNbt(nbt, this.inventory.getStacks());
    }

    @Override
    public World getWorld() {
        return this.player.getWorld();
    }

    @Override
    public byte getScreenID() {
        return this.screenID;
    }

    @Override
    public ItemStack getItemStack() {
        return this.stack == null ? ItemStack.EMPTY : this.stack;
    }

    @Override
    public void setUsingPlayer(@Nullable PlayerEntity player) { }

    @Override
    public void markDataDirty(byte... dataIds) {
        if (this.getWorld().isClient() || this.stack == null) return;

        for (byte data : dataIds) {
            switch (data) {
                case INVENTORY_DATA -> CancerMeterInventory.writeNbt(this.stack.getOrCreateNbt(), this.getInventory().getStacks(), true);
                case COMBINED_INVENTORY_DATA -> writeItems(stack.getOrCreateNbt());
                case ALL_DATA -> writeAllData(stack.getOrCreateNbt());
            }
        }
    }

    @Override
    public void markDirty() { }

    public static void openHandledScreen(PlayerEntity player, ItemStack stack, byte screenID) {
        if (!player.getWorld().isClient()) {
            player.openHandledScreen(new ExtendedScreenHandlerFactory() {
                @Override
                public void writeScreenOpeningData(ServerPlayerEntity player, PacketByteBuf buf) {
                    buf.writeByte(screenID);
                }

                @Override
                public Text getDisplayName() {
                    return Text.translatable("screen.asbestos.cancer_meter");
                }

                @Nullable
                @Override
                public ScreenHandler createMenu(int syncId, PlayerInventory playerInventory, PlayerEntity player) {
                    return new CancerMeterScreenHandler(syncId, playerInventory, new CancerMeterInventory(stack, player, screenID));
                }
            });
        }
    }

    private InventoryImproved createInventory(int size) {
        return new InventoryImproved(DefaultedList.ofSize(size, ItemStack.EMPTY)) {
            @Override
            public void markDirty() {
                markDataDirty(COMBINED_INVENTORY_DATA);
            }
        };
    }

    private static NbtCompound writeNbt(NbtCompound nbt, DefaultedList<ItemStack> stacks, boolean setIfEmpty) {
        NbtList nbtList = new NbtList();
        for (int i = 0; i < stacks.size(); ++i) {
            ItemStack itemStack = stacks.get(i);
            if (!itemStack.isEmpty()) {
                NbtCompound nbtCompound = new NbtCompound();
                nbtCompound.putByte("Slot", (byte)i);
                itemStack.writeNbt(nbtCompound);
                nbtList.add(nbtCompound);
            }
        }

        if (!nbtList.isEmpty() || setIfEmpty) {
            nbt.put("Inventory", nbtList);
        }

        return nbt;
    }

    private static void readNbt(NbtCompound nbt, DefaultedList<ItemStack> stacks) {
        NbtList nbtList = nbt.getList("Inventory", 10);

        for(int i = 0; i < nbtList.size(); ++i) {
            NbtCompound nbtCompound = nbtList.getCompound(i);
            int j = nbtCompound.getByte("Slot") & 255;
            if (j >= 0 && j < stacks.size()) {
                stacks.set(j, ItemStack.fromNbt(nbtCompound));
            }
        }
    }
}
