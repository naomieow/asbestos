package xyz.naomieow.asbestos.inventory;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.world.World;

public interface ICancerMeterInventory {
    void writeItems(NbtCompound nbt);
    void readItems(NbtCompound nbt);
    void writeAllData(NbtCompound nbt);
    void readAllData(NbtCompound nbt);
    InventoryImproved getInventory();
    byte getScreenID();
    World getWorld();
    ItemStack getItemStack();
    void setUsingPlayer(PlayerEntity player);

    byte INVENTORY_DATA = 0;
    byte COMBINED_INVENTORY_DATA = 1;
    byte SLOT_DATA = 2;
    byte ALL_DATA = 3;

    void markDataDirty(byte... dataIds);
    void markDirty();
}
